#!/usr/bin/env python

# TODO: straight with ACE may be low
# TODO: consider kicker


class Combination:
    COMBINATION_NAMES = ['Royal Flash',
                         'Straight Flush',
                         'Four of a Kind',
                         'Full House',
                         'Flush',
                         'Straight',
                         'Three',
                         'Two Pairs',
                         'Pair',
                         'Highest']

    def __init__(self, cards=None):
        self.cards = cards if cards is not None else []
        self.index = 255
        self.combination_cards = []
        self.name = ''

    def add_card(self, card):
        self.cards.append(card)

    def __add__(self, card):
        self.add_card(card)

    def reset(self):
        self.cards = []
        self.index = 255
        self.combination_cards = []
        self.name = ''

    @property
    def name(self):
        self.get_max_combination()
        return self.name

    @property
    def combination_cards(self):
        self.get_max_combination()
        return self.combination_cards()

    def get_max_combination(self):
        """ all functions checking combinations must take sorted list on input """
        def check_royal_flush(cards):
            """ check if royal flush
            :param cards:
            """
            cards = check_straight_flush(cards)

            if len(cards) == 5 and cards[4].split('-')[1] == '14':
                return cards
            return []

        def check_straight_flush(cards):
            """ check if straight flush
              :param cards:
              :rtype: list
              """
            prev_card = cards.pop(0)
            pack = [prev_card]
            for card in cards:
                if card.split('-')[0] == prev_card.split('-')[0] and \
                                int(card.split('-')[1]) == int(prev_card.split('-')[1]) + 1:
                    pack.append(card)
                else:
                    if len(pack) >= 5:
                        break
                    pack = [card]
                prev_card = card

            if len(pack) >= 5:
                return pack[-5:]

            return []

        def check_four(cards):
            """ check if four
                  :param cards:
                  """
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if base_card == cmp_card:
                        continue
                    if base_card.split('-')[1] == cmp_card.split('-')[1]:
                        pack.append(cmp_card)
                    if len(pack) == 4:
                        return pack
            return []

        def check_full_house(cards):
            """ check if full house
                  :param cards:
                  """  # find all threes and pairs
            pairs = []
            threes = []
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if base_card == cmp_card:
                        continue
                    if cmp_card.split('-')[1] == base_card.split('-')[1]:
                        pack.append(cmp_card)
                        if len(pack) == 2:
                            wasBefore = False
                            for cmp_pack in pairs:
                                if cmp_pack[0].split('-')[1] == cmp_card.split('-')[1]:
                                    wasBefore = True
                            if not wasBefore:
                                pairs.append(pack[:])
                        if len(pack) == 3:
                            wasBefore = False
                            for cmp_pack in threes:
                                if cmp_pack[0].split('-')[1] == cmp_card.split('-')[1]:
                                    wasBefore = True
                            if not wasBefore:
                                threes.append(pack[:])
                            break

            if len(threes) == 0:
                return []
            if len(pairs) < 2:
                return []

            # best full house combination
            best_three = threes.pop()
            if len(threes):
                three = threes.pop()
                if three[0].split('-')[1] > best_three[0].split('-')[1]:
                    best_three = three
            best_pair = pairs.pop()
            if best_pair[0].split('-')[1] == best_three[0].split('-')[1]:
                best_pair = pairs.pop()
            if len(pairs):
                pair = pairs.pop()
                if pair[0].split('-')[1] > best_pair[0].split('-')[1]:
                    best_pair = pair

            return best_three + best_pair

        def check_flush(cards):
            """ check if flush
                  :param cards:
                  """
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if base_card == cmp_card:
                        continue
                    if cmp_card.split('-')[0] == base_card.split('-')[0]:
                        pack.append(cmp_card)

                if len(pack) >= 5:
                    return pack[-5:]
            return []

        def check_straight(cards):
            cards.sort(key=lambda x: (x[2], x[3]))  # sort by nominal
            """ check if straight """
            for base_card in cards:
                pack = [base_card]
                prev_card = base_card
                for cmp_card in cards:
                    if base_card == cmp_card:
                        continue
                    if int(cmp_card.split('-')[1]) == int(prev_card.split('-')[1]) + 1:
                        pack.append(cmp_card)
                        prev_card = cmp_card

                if len(pack) >= 5:
                    return pack[-5:]

            return []

        def check_three(cards):
            """ check if three
                  :param cards:
                  """
            """ find only first threes, cause if there are two threes, it's FH """
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if base_card == cmp_card:
                        continue
                    if cmp_card.split('-')[1] == base_card.split('-')[1]:
                        pack.append(cmp_card)
                        if len(pack) == 3:
                            return pack

            return []

        def check_two_pairs(cards):
            """ check if two pairs
                  :param cards:
                  """
            cards.sort(key=lambda x: (x[2], x[3]))  # sort by nominal
            pairs = []
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if cmp_card == base_card:
                        continue
                    if cmp_card.split('-')[1] == base_card.split('-')[1]:
                        pack.append(cmp_card)
                        wasBefore = False
                        # check if this card was before
                        for cmp_pack in pairs:
                            if cmp_pack[0].split('-')[1] == cmp_card.split('-')[1]:
                                wasBefore = True
                                continue
                        # add this pack if pair with this nominal wasn't added before
                        if not wasBefore:
                            pairs.append(pack[:])
                        break
            if len(pairs) > 2:
                return pairs[-1] + pairs[-2]
            return []

        def check_pair(cards):
            """ check if pair
                  :param cards:
                  """
            pairs = []
            for base_card in cards:
                pack = [base_card]
                for cmp_card in cards:
                    if cmp_card == base_card:
                        continue
                    if cmp_card.split('-')[1] == base_card.split('-')[1]:
                        pack.append(cmp_card)
                        wasBefore = False
                        # check if this card was before
                        for cmp_pack in pairs:
                            if cmp_pack[0].split('-')[1] == cmp_card.split('-')[1]:
                                wasBefore = True
                                continue
                        # add this pack if this nominal wasn't added before
                        if not wasBefore:
                            pairs.append(pack[:])
                            break
            if len(pairs) > 1:
                return pairs[-1]
            return []

        def check_high_card(cards):
            """ check high card
                  :param cards:
                  """
            high_card = cards.pop()
            for card in cards:
                if card.split('-')[1] > high_card.split('-')[1]:
                    high_card = card
            return [high_card]

        functions = [check_royal_flush,
                     check_straight_flush,
                     check_four,
                     check_full_house,
                     check_flush,
                     check_straight,
                     check_three,
                     check_two_pairs,
                     check_pair,
                     check_high_card]

        for idx, checkFunction in enumerate(functions):
            combination = checkFunction(self.cards[:])
            if len(combination) > 0:
                self.combination_cards = combination[:]
                self.combination_index = idx
                self.combination_name = self.COMBINATION_NAMES[idx]
                return self.combination_cards

    def __compare_combination(self, other_comb):
        """ returns 1 if player's combination is greater, 0 if equal, -1 if less """
        if self.combination_index > other_comb.combination_index:
            return -1
        if self.combination_index < other_comb.combination_index:
            return 1
        # if combinations are the same
        combination = self.combination_index

        if self.combination_index == 0:  # royal flush
            return 0

        if combination == 1:  # straight flush
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            return self.__get_highest(highest1, highest2)

        if combination == 2:  # four of a kind
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            return self.__get_highest(highest1, highest2)

        if combination == 3:  # full house, threes come first, compare them
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            if highest1 > highest2:
                return 1
            elif highest1 < highest2:
                return -1
            elif highest1 == highest2:  # compare twos
                highest1 = self.combination_cards[3].split('-')[1]
                highest2 = other_comb.combination_cards[3].split('-')[1]
                return self.__get_highest(highest1, highest2)

        if combination == 4:  # flush
            for idx, card in enumerate(self.combination_cards):  # check all cards
                highest1 = card.split('-')[1]
                highest2 = other_comb.combination_cards[4 - idx].split('-')[1]
                result = self.__get_highest(highest1, highest2)
                if result != 0:
                    return result
            return 0

        if combination == 5:  # straight
            highest1 = self.combination_cards[4].split('-')[1]
            highest2 = other_comb.combination_cards[4].split('-')[1]
            return self.__get_highest(highest1, highest2)

        if combination == 6:  # threes
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            return self.__get_highest(highest1, highest2)

        if combination == 7:  # threes
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            return self.__get_highest(highest1, highest2)

        if combination == 8:  # pairs
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            result = self.__get_highest(highest1, highest2)
            if result == 0:
                highest1 = self.combination_cards[2].split('-')[1]
                highest2 = other_comb.combination_cards[2].split('-')[1]
                return self.__get_highest(highest1, highest2)

        if combination == 9:  # high card
            highest1 = self.combination_cards[0].split('-')[1]
            highest2 = other_comb.combination_cards[0].split('-')[1]
            return self.__get_highest(highest1, highest2)

        return 0

    def __get_highest(self, highest1, highest2):
        if highest1 > highest2:
            return 1
        elif highest1 < highest2:
            return -1
        return 0

    """ rich comparison operators """
    def __gt__(self, other):
        if self.__compare_combination(other) == 1:
            return True
        return False

    def __ge__(self, other):
        if self.__compare_combination(other) >= 0:
            return True
        return False

    def __lt__(self, other):
        if self.__compare_combination(other) == -1:
            return True
        return False

    def __le__(self, other):
        if self.__compare_combination(other) <= 0:
            return True
        return False

    def __eq__(self, other):
        if self.__compare_combination(other) == 0:
            return True
        return False

    def __ne__(self, other):
        if self.__compare_combination(other) != 0:
            return True
        return False
