#!/usr/bin/env python
from pot import Pot


class PotManager:
    def __init__(self):
        self.pots = []

    def reset(self, players=None):
        self.pots.clear()
        self.pots.add_pot(Pot(players))

    def add_player(self, player):
        self.pots.index()

    def add_pot(self, players):
        self.pots.append(Pot(players))

    def remove_player(self, player):
        for pot in self.pots:
            pot.remove_player(player)

    @property
    def winners(self):
        for pot in self.pots:
            return [pot.winners, pot.pot, pot.pot_per_player, pot.remainder]





