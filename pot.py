#!/usr/bin/env python
from player import Player
from uuid import uuid4


class Pot:
    def __init__(self, players=None):
        self.pot = 0
        self.id = uuid4()
        self.players = []
        self.winners = []
        for player in players:
            self.players.append(player)

    def add(self, player, stake):
        self.pot += stake
        if player not in self.players:
            self.players.append(player)

    def remove_player(self, player):
        self.players.remove(player)

    @property
    def winners(self):
        if not self.players:
            return -1
        self.players = sorted(self.players, key=lambda x:x, reversed=True)
        pl_copy = self.players[:]
        self.winners.append(pl_copy[0])
        pl_copy.remove(0)
        while(pl_copy):
            if self.winners[0] > pl_copy[0]:
                break
            self.winners.append(pl_copy[0])
            pl_copy.remove(0)
        return self.winners

    @property
    def pot(self):
        return self.pot

    @property
    def pot_per_player(self):
        if len(self.winners) == 0:
            return 0
        return self.pot // len(self.winners)

    @property
    def remainder(self):
        if len(self.winners) == 0:
            return 0
        return self.pot % len(self.winners)

