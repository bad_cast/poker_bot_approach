#!/usr/bin/env python
# coding: utf8

import uuid
from combination import Combination


class Player:
    def __init__(self, id=0, name='', player_cards=None):
        self.name = name
        self.id = id if id != 0 else uuid.uuid4()
        self.player_cards = player_cards if player_cards is not None else []
        self.table_cards = []
        self.combination = Combination()

    def add_player_card(self, card):
        self.player_cards.append(card)
        self.combination += card

    def add_table_card(self, card):
        self.table_cards.append(card)
        self.combination += card

    def reset(self):
        self.player_cards.clear()
        self.table_cards.clear()
        self.combination.reset()


