#!/usr/bin/env python

from player import Player
from tools.AutoNumber import AutoNumber


class PlayerStatus(AutoNumber):
    NOT_IN_GAME = ()
    IN_GAME_INACTIVE = ()
    IN_GAME = ()
    WAITING = ()
    WAITING_ALL_IN = ()
    FOLD_WAITING = ()


class PlayerAction(AutoNumber):
    NO_ACTION = ()
    BET = ()
    FOLD = ()
    CHECK = ()
    RAISE = ()
    CALL = ()
    ALL_IN = ()
    LEAVE = ()


class PlayerRole(AutoNumber):
    NOT_IN_GAME = ()
    DEALER = ()
    SMALL_BLIND = ()
    BIG_BLIND = ()
    FORCED_BET = ()

StatusTransitions =[[0, 1, 1, 0, 0], # NOT_IN_GAME
                    [1, 0, 1, 0, 0], # IN_GAME_INACTIVE
                    [1, 1, 0, 1, 0], # IN_GAME
                    [1, 1, 0, 0, 0], # WAITING
                    [1, 1, 0, 0, 0]] # FOLD_WAITING


class PlayerTrader(Player):
    def __init__(self, id=0, name='', balance=1000, player_cards=None):
        Player.__init__(self, id, name, player_cards)
        self.balance = balance
        self.status = PlayerStatus.NOT_IN_GAME
        self.role = PlayerRole.NOT_IN_GAME
        self.action = PlayerAction.NO_ACTION
        self.last_stake = 0

    def enter_table(self, stake, status, role):
        if stake > self.balance:
            return False
        possible_stake = 0
        self.role = role
        self.status = PlayerStatus.IN_GAME
        if role == 'BIG_BLIND' or role == 'FORCED_BET':
            self.balance -= stake
            self.last_stake = stake
        if role == 'SMALL_BLIND':
            self.balance -= int(stake/2)
            self.last_stake = int(stake/2)
        return self.last_stake

    def leave_table(self):
        self.role = PlayerRole.NOT_IN_GAME
        self.status = PlayerStatus.NOT_IN_GAME
        self.action = PlayerAction.NO_ACTION
        self.last_stake = 0

    def leave_round(self):
        self.role = PlayerRole.NOT_IN_GAME
        self.status = PlayerStatus.IN_GAME_INACTIVE
        self.action = PlayerAction.NO_ACTION

    def place_stake(self, stake, action):
        if self.balance <= stake:
            self.action = PlayerAction.ALL_IN
            self.last_stake = self.balance
            self.balance = 0
            self.status = PlayerStatus.WAITING_ALL_IN
        else:
            self.action = action
            self.last_stake = stake
            self.balance -= stake
            self.status = PlayerStatus.WAITING

    def bet(self, stake):
        self.PlaceStake(self, stake, PlayerAction.BET)

    def call(self, stake):
        self.PlaceStake(self, stake, PlayerAction.CALL)

    def raise_(self, stake):
        self.PlaceStake(self, stake, PlayerAction.RAISE)

    def check(self):
        self.action = PlayerAction.CHECK
        self.last_stake = 0
        self.status = PlayerStatus.WAITING

    def fold(self):
        self.action = PlayerAction.FOLD
        self.last_stake = 0
        self.status = PlayerStatus.FOLD_WAITING