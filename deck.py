#!/usr/bin/env python

import random


class Deck:
    CARD_VALUES = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']
    CARD_NUMBERS = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]
    CARD_SUITS = ['H', 'S', 'C', 'D']  # Hearts, Spades, Clubs, Diamonds

    def __init__(self):
        self.cards = []
        self.used_cards = []
        random.seed()
        self.reset()

    def reset(self):
        self.cards = []
        for cardNom in self.CARD_NUMBERS:
            for cardSuit in self.CARD_SUITS:
                self.deck.append(cardSuit + '-' + ('%02d' % cardNom))
        random.shuffle(self.deck)

    def get_card(self):
        card = self.cards.pop()
        self.used_cards.append(card)
        return card
