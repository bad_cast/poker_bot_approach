#!/usr/bin/env python

from tools.AutoNumber import AutoNumber
from player_trader import PlayerTrader
from deck import Deck


class TableRound(AutoNumber):
    NOT_STARTED = ()
    PREFLOP = ()
    FLOP = ()
    TURN = ()
    RIVER = ()
    OPEN_CARDS = ()


class Table:
    def __init__(self):
        self.deck = Deck()
        self.table_cards = []
        self.players = {}
        self.step = TableRound.NOT_STARTED

    def reset(self):
        self.deck.reset()
        self.table_cards.clear()
        self.players.clear()
        self.step = TableRound.NOT_STARTED

    def add_player(self, name, id=0, balance=1000):
        player = PlayerTrader(name=name, id=id, balance=balance)
        self.players[player.id] = player
        return player

    def deal_cards(self):
        for player in self.players:
            player.player_cards.append(self.deck.get_card())
        for player in self.players:
            player.player_cards.append(self.deck.get_card())
        self.step = TableRound.PREFLOP

    def get_flop(self):
        # skip first
        self.deck.get_card()
        self.table_cards.append(self.deck.get_card())
        self.table_cards.append(self.deck.get_card())
        self.table_cards.append(self.deck.get_card())
        self.step = TableRound.FLOP

    def get_turn(self):
        # skip first
        self.deck.get_card()
        self.table_cards.append(self.deck.get_card())
        self.step = TableRound.TURN

    def get_river(self):
        self.get_turn()
        self.step = TableRound.RIVER

    def get_winner(self):
        pass
